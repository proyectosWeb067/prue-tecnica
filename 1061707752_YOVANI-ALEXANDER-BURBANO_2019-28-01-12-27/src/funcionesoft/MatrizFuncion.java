/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funcionesoft;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

/**
 *
 * @author yio
 */
public class MatrizFuncion {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("ingrese las filas: ");
        int f = sc.nextInt();

        System.out.print("ingrese las columnas: ");
        int c = sc.nextInt();

        //----------Matriz recibe dos parametros f y c--------------
        System.out.println(Matriz(f, c));
        //----------------------------------------------------------
    }

    public static String Matriz(int f, int c) {
        String mensajeOut;
        if ((f <= 2 || c <= 2) || (f <= 2 && c <= 2)) {
            System.out.println("\n");
            mensajeOut = "Las filas y columnas deben ser mayores a 2, intente de nuevo. ";
            return mensajeOut;
        } else {

            mensajeOut = "¡Gracias!";

            int[][] matriz = new int[f][c];
            int a = (f * c);
            int[] a1 = new int[a];
            int nn;
            int iii = 0;
            int aument = 0;
            int cont1 = 1;
            int cont2 = 0;
            int cont4 = 0;
            int pp;
            int cont5 = 0;
            int num;
            int cont = 1;
            int[] numeros = new int[a];
            int[] aa = new int[a];
            int[] arreglOut = new int[a];
            int iteraNu = 0;
            int contindiceArray = 0;
            int numeroIn = +0;

            Integer sumT1 = 0, sumT2 = 0, sumT3 = 0;
            TreeSet<Integer> numAle = new TreeSet<>();
            TreeSet<Integer> unionNumeros = new TreeSet<>();
            List<Integer> arrlist = new ArrayList();

            while (cont1 <= a) {
                num = (int) ((Math.random() * a) + 1);

                numAle.add(num);
                cont1++;
            }

            Iterator<Integer> ite = numAle.iterator();
            while (ite.hasNext()) {
                nn = ite.next();

                aa[cont2] = nn;
                cont2++;

                // System.out.println("num iterador-->" + nn);
            }

            for (int l = 0; l < a; l++) {

                numeros[l] = cont;
                cont++;

                //System.out.println("numeros fijos-->" + numeros[l]);
            }

            for (int az = 0; az <= numAle.size(); az++) {
                for (int ax = 0; ax <= (a - 1); ax++) {

                    if (aa[az] == numeros[ax]) {
                        // System.out.println("numeros iguales de cada array--> " + aa[az]);
                        unionNumeros.add(aa[az]);
                    } else {
                        // System.out.println("numeros diferentes de cada array-->" + numeros[ax]);
                        unionNumeros.add(numeros[ax]);
                    }

                }
            }
            Iterator<Integer> iteratt = unionNumeros.iterator();

            while (iteratt.hasNext()) {
                pp = iteratt.next();
                //System.out.println("la lista okidoki---->" + pp);
                arreglOut[cont5] = pp;

                cont5++;
            }

            //--------------------for Matriz------------------   
            for (int i = 0; i < matriz.length; i++) {

                for (int j = 0; j < matriz[i].length; j++) {

                    matriz[i][j] = arreglOut[aument];
                    arrlist.add(matriz[i][j]);

                    aument++;
                }

            }

            //me permite desordenar los numeros agregados a la lista
            Collections.shuffle(arrlist);

            Iterator<Integer> it = arrlist.iterator();
            while (it.hasNext()) {
                iteraNu = it.next();
                a1[cont4] = iteraNu;
                cont4++;
            }

            for (int head = 0; head < matriz.length; head++) {
                System.out.print(" c" + head + "|");

            }
            System.out.println("");

            for (int i = 0; i < matriz.length; i++) {
                System.out.print("[ ");

                for (int j = 0; j < matriz[i].length; j++) {

                    matriz[i][j] = a1[contindiceArray];

                    int sp = matriz[i][j];

                    //dandole un espacio en blanco para acomodar matriz
                    if (Integer.toString(sp).length() <= 1) {

                        System.out.print(" " + matriz[i][j] + " ");

                    } else {
                        System.out.print(matriz[i][j] + " ");
                    }

                    contindiceArray++;
                }
                System.out.println(" ]");
            }
            System.out.println("");
            int ka, lq, ts1 = 0, ts2 = 0, ts3 = 0, sum = 0, sumpares = 0;
            for (ka = 0; ka < matriz.length; ka++) {
                for (lq = 0; lq < matriz[ka].length; lq++) {
                    numeroIn = matriz[ka][lq];
                    switch (lq) {
                        case 0:

                            sumT1 = sumT1 + numeroIn;
                            ts1 = sumT1;

                            break;
                        case 1:
                            sumT2 = sumT2 + numeroIn;
                            ts2 = sumT2;

                            break;
                        case 2:
                            sumT3 = sumT3 + numeroIn;
                            ts3 = sumT3;

                            break;

                    }
                }

                System.out.println("");

            }
            System.out.println("Totales columnas: " + ts1 + "," + ts2 + "," + ts3);
            sum = ts1 + ts2 + ts3;
            int contaPar = 0;
             if(ts1%2 == 0){
                 sumpares = sumpares +ts1;
                 contaPar++;
             }
             if(ts2%2 == 0){
                 sumpares = sumpares +ts2;
                 contaPar++;
             }
             if(ts3%2 == 0){
                 sumpares = sumpares +ts3;
                 contaPar++;
             }
              
            System.out.println("El promedio de columnas pares es: "+(sumpares / contaPar));
            System.out.print("La suma de los elementos_total columna: " + sum);
        }

        System.out.println("\n");
        return mensajeOut;
    }

}
